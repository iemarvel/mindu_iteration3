﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MindU_I2.Models
{
    public class RSInput
    {
        public Boolean isForMe { get; set;}
        public Boolean likeArt { get; set;}
        public Boolean likeSport {get; set;}
        public Boolean needPH { get; set;}
        public Boolean needInfo { get; set;}
    }
}