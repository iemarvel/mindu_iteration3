﻿BULK
INSERT SportRating
FROM 'd:\sport_rating.txt'
WITH
(
FIRSTROW = 2,
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n'
)
GO