﻿BULK
INSERT SportImages
FROM 'd:\sport_images.txt'
WITH
(
FIRSTROW = 2,
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n'
)
GO