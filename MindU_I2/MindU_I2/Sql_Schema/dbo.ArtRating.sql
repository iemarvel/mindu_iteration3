﻿CREATE TABLE [dbo].[ArtRating] (
    [rid]      INT           NOT NULL,
    [likes]    VARCHAR (100) NOT NULL,
    [dislikes] VARCHAR (100) NOT NULL,
    [reviews]  VARCHAR (100) NOT NULL,
    [aid]      INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([rid] ASC),
    CONSTRAINT [FK_ArtRating_ToArts] FOREIGN KEY ([aid]) REFERENCES [dbo].[Arts] ([aid])
);

