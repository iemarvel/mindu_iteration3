﻿CREATE TABLE [dbo].[SportRating] (
    [rid]      INT           NOT NULL,
    [likes]    VARCHAR (100) NOT NULL,
    [dislikes] VARCHAR (100) NOT NULL,
    [reviews]  VARCHAR (100) NOT NULL,
    [sid]      INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([rid] ASC),
    CONSTRAINT [FK_SportRating_ToSports] FOREIGN KEY ([sid]) REFERENCES [dbo].[Sports] ([sid])
);

