﻿using MindU_I2.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MindU_I2.Controllers
{
    public class HomeController : Controller
    {
        private MindUEntities db = new MindUEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {

            return View();
        }
        public ActionResult HelpLine()
        {
            return View();
        }

        public ActionResult TestCard()
        {
            return View();
        }
 
      
        [HttpPost]
        public ActionResult Result(RSInput input)
        {
            //initialise the models and ViewBag values that would be returned to the view.
            List<Art> arts = null;
            List<Sport> sports = null;
            ViewBag.needPH = false;
            ViewBag.needInfo = false;

            if (input.likeArt == true) {
            //get a list of art ratings
            List<ArtRating> aRatings = db.ArtRatings.ToList();
            //get the top-3 aids based on the no. of likes
            var aIds = aRatings.OrderByDescending(r => int.Parse(r.likes)).Select(r => r.aid).Take(3).ToList();
            //find the top-3 art items from the arts table
            arts = db.Arts.Where(a => aIds.Contains(a.aid)).ToList();
            }
            if (input.likeSport) {

                //get a list of sport ratings
                List<SportRating> sRatings = db.SportRatings.ToList();
                //get the top-3 sids based on the no. of likes
                var sIds = sRatings.OrderByDescending(r => int.Parse(r.likes)).Select(r => r.sid).Take(3).ToList();
                //find the top-3 sport items from the arts table
                sports = db.Sports.Where(s => sIds.Contains(s.sid)).ToList();
            }
            if (input.needPH == true) {
                ViewBag.needPH = true;
            }
            if (input.needInfo == true)
            {
                ViewBag.needInfo = true;
            }
            //return a tuple containing two models back to the view
            return View(Tuple.Create(arts, sports));
        }
    }

}