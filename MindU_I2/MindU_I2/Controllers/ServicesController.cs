﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MindU_I2.Models;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace MindU_I2.Controllers
{
    public class ServicesController : Controller
    {

  
        public ActionResult ConditionsInfo()
        {
            return View();
        }

        public ActionResult MentalHealthContext()
        {
            return View();
        }

        public ActionResult Tips()
        {
            return View();
        }

        public ActionResult YoungPeopleMentalHealth()
        {
            return View();
        }    
        public ActionResult Search(string category)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Please choose category", Value = "Please choose category" });
            items.Add(new SelectListItem { Text = "Psychological Consultant", Value = "Psychological Consultant" });
            items.Add(new SelectListItem { Text = "Psychological Clinic", Value = "Psychological Clinic" });
            items.Add(new SelectListItem { Text = "Psychologist", Value = "Psychologist" });
            ViewBag.defaultSelect = "Please choose category";
            ViewBag.selectList = items;
            // Display SearchResult.html page that shows results associated with selected category.
            string apiKey = "AIzaSyC-XG2JrTbessPRoSMRbasmtCwBY8oR2iI";
            var venues = new List<Venue>();
            if (category == null)
            {
                ViewBag.result = 0;
                return View(venues);

            }
            if (category.Contains(" "))
            {
                category.Replace(" ", "+");
            }
            var request = WebRequest.Create("https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + category + "+in+Vic" + "&key=" + apiKey);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseString = reader.ReadToEnd();
            dynamic jsonData = JsonConvert.DeserializeObject(responseString);
            //var venues = new List<Venue>();
            if (jsonData.results == null)
            {
                ViewBag.result = 1;
                return View(venues);
            }
            foreach (var item in jsonData.results)
            {
                string ratingx = item.rating;
                if (ratingx.Contains("5"))
                {
                    venues.Add(new Venue
                    {
                        name = item.name,
                        formatted_address = item.formatted_address,
                        rating = item.rating,
                        place_id = item.place_id,
                        lat = item.geometry.location.lat,
                        lng = item.geometry.location.lng
                    });
                }
            }

            foreach (var venue in venues)
            {
                request = WebRequest.Create("https://maps.googleapis.com/maps/api/place/details/json?placeid=" + venue.place_id + "&fields=formatted_phone_number,website&key=" + apiKey);
                response = (HttpWebResponse)request.GetResponse();
                dataStream = response.GetResponseStream();
                reader = new StreamReader(dataStream);
                responseString = reader.ReadToEnd();
                jsonData = JsonConvert.DeserializeObject(responseString);
                venue.formatted_phone_number = jsonData["result"]["formatted_phone_number"];
                venue.website = jsonData["result"]["website"];

            }
            return View(venues);
        }
        }
    }
